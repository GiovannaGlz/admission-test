#### Frontend Dev

##### Aptitudes
* Autónomo
* Pro-eficiente
* Pro-activo
* Trabajo en equipo
* Responsable

##### Requisitos
* Sólidos conocimientos en JavaScript y ES6
* Sólidos conocimientos en CSS, Flexbox, Media Querys etc
* React Native
* Manejo de estados con Redux, Async, Middlewares
* Experiencia construyendo UI
* Uso de Mejores prácticas y Estándares de desarrollo (ESLint)
* GIT
* Agile methodologies
* Package Managers (Bower, NPM, Yarn)
* Pruebas unitarias
* Experiencia con Google Play Store y App Store

##### Plus
* TypeScript 
* NodeJs
* Graphql
* Less / Sass
* Docker
