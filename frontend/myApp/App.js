import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component{

  constructor(props){
    super(props);

    this.state={
      loading: false,
      search: [],
      url: 'https://api.github.com/search/'
    }
  }

  componentDidMount(){
    this.getSearch();
  }

  getSearch =() =>{

    fetch(this.state.url)
    .then(res => res.json())
    .then( res =>{

      this.setState({
        search: res.result,
        url: res.next,
        loading: false
      })
    });

  };

  render(){
    if(this.state.loading){
      return (
        <View style={styles.container}>
          <Text>Buscando</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Text>Lista de resultados</Text>
      </View>
    );

    
  }
  }
  

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: "transparent",
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    margin: 2
  },
});
