<img src="img/logo.png" width="75px" alt="Fuddie Logo">

# Fuddie Admission Test

Fuddie is looking for members who are interested in being part of an agile, progressive, proactive team with new ideas.

We seek to use technology as an ally to link our objectives as well as to offer customers a better user experience as well as delivering added value in our products.

### Tests:
* [Backend Developer Test](backend/index.md)
* [Frontend Developer Test](frontend/index.md)
