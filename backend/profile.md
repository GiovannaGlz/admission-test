#### Backend Dev

##### Aptitudes
* Autónomo
* Pro-eficiente
* Pro-activo
* Trabajo en equipo
* Responsable

##### Requisitos
* PHP >= 7.1
* Dominio en Programación Orientada a Objetos
* Principios S.O.L.I.D.
* Patrones de Diseño
* Diseño de Web Services (Soap, Rest)
* Dominio en algún Backend Frameworks (Symfony3+, Laravel5+, CodeIgnitier, Zend, etc)
* Uso de Mejores prácticas y Estándares de desarrollo (PSR's)
* DataBases SQL (MySQL, MariaDB)
* GIT
* Agile methodologies
* Package Managers (Composer)
* Unit testing (PHPUnit, Behat)

##### Plus
* Docker
* EC2 y S3 o algunos otros servicios de AWS
* NodeJs
* Diseño y experiencia con Microservicios
