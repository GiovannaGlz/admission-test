#### Backend Jr Dev

##### Aptitudes
* Autónomo
* Pro-eficiente
* Pro-activo
* Trabajo en equipo
* Responsable

##### Requisitos
* PHP >= 7.1
* Experiencia creando/consumiendo Web Services (Rest)
* Experiencia con algún Framework (Symfony3+, Laravel5+, CodeIgnitier, Zend, etc)
* Uso de Mejores prácticas y Estándares de desarrollo (PSR's)
* Conocimientos en conceptos de Programación Orientada a Objetos
* DataBases SQL (MySQL, MariaDB)
* GIT
* Agile methodologies
* Package Managers (Composer)
* Unit testing (PHPUnit, Behat)

##### Plus
* Principios S.O.L.I.D.
* Patrones de Diseño
